PROJECT=presentacion
LANG=es

.PHONY: all clean check

all: $(PROJECT).pdf

# MAIN LATEXMK RULE
$(PROJECT).pdf: $(PROJECT).tex
	latexmk -f -bibtex -use-make -pdf -pdflatex="pdflatex -synctex=1" $(PROJECT).tex

check: $(PROJECT).tex
	aspell --lang=$(LANG) -t -c $(PROJECT).tex

indent: $(PROJECT).tex
	latexindent --overwrite $<

clean:
	latexmk -C




